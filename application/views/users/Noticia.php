<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/Noticia.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="arriba">
        <img class="logo" src="<?php echo base_url()?>img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button style="margin-left: 900px; margin-top: 20px" class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php 
                    $user = $_SESSION['usuarios'];
                    foreach($user as $row){
                        echo($row['nombre']);
                    }
                ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo site_url(['user','categoria']);?>">Categories</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','noticia']);?>">News Sources</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','login']);?>">Logout</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="texto1">
            <h1 class="text">News Sources</h1>
            <hr>
        </div>

        <div>

            <center>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="datos">
                    <?php
                    foreach($nombre as $row){?>
                        <tr>
                            <td><?php echo $row['nombre'];?></td>
                            <td><?php echo $row['categoria'];?></td>
                            <td>
                                <a href="<?php echo site_url(['news','chargeNewsById',$row['id']]);?>"><button type="button"
                                        class="btn btn-success">Edit</button></a>
                                <a href="<?php echo site_url(['news','delete',$row['id']]);?>"><button type="button"
                                        class="btn btn-danger">Delete</button></a>
                            </td>
                        </tr>
                    </tbody>
                    <?php
        }
        ?>

                </table>
            </center>
        </div>

        <div class="cuerpo">

            <a class="button" href="<?php echo site_url(['user','agregarFuenteNoticia']);?>">Add New</a>
            <br><br><br><br><br><br>
        </div>
    </div>

    <hr>
    <div class="footer">
        <div class="fintxt">
            <a>MyCover</a>
            <a href="">|</a>
            <a>About</a>
            <a href="">|</a>
            <a>Help</a>
        </div>
    </div>
    <br>
    <p style="margin-right: 60px" class="final"><span class="logo">&copy;</span>My News Cover</p>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>