
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/Principal.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

        <script type="text/javascript">
    function checkExistingEmail($filter) {
      if ( $filter.length > 5 ) { // ideally validate if its a valid email address with a regex
        const xhttp = new XMLHttpRequest(); //creates the XMLHttpRequest object
        xhttp.onreadystatechange = function() { //function called when request is completed
          // do whatever you want with the response received from the server
          if (this.readyState == 4 && this.status == 200) {
            const response = JSON.parse(this.responseText);
            if (response.status) {
              document.getElementById('buscar');
            } else {
              document.getElementById('buscar');
            }
          }
        };

        xhttp.open("POST", "/index.php/news/chargeNewsByFilter/"+$filter, true); //define where should the request be made to
        xhttp.send(); // execute the call
      }
    }
  </script>
</head>

<body>

    <div class="arriba">
        <img class="logo" src="<?php echo base_url()?>img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php 
                    $user = $_SESSION['usuarios'];
                    foreach($user as $row){
                        echo($row['nombre']);
                    }
                ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo site_url(['user','categoria']);?>">Categories</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','noticia']);?>">News Sources</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','login']);?>">Logout</a>
            </div>
        </div>
    </div>
    


    <div class="container">
    <form method="POST" action="<?php echo site_url(['news','chargeNews']);?>">
            <button style="" type="submit">Recargar noticias</button>
        </form>
        <div class="texto1">
            <h1 class="text">Your Unique News Cover</h1>
            <hr class="first">
            <br>
        </div>

        <div class="cuadritos">
            <?php
            $categoria = $this->News_model->getAllCategories();
            $data['categoria'] = $categoria;
            foreach($categoria as $row){
            ?>
            <div class="card" style="width: 12rem;">
                <ul class="list-group list-group-flush">
                    <form method="post" >
                        <input style="visibility: hidden;" type="text" name="getId" placeholder=""
                            value="<?php echo $row['id']?>">
                        <a href=""><button type="submit" name="submit" class="list-group-item"
                                name="botonCategoria"><?php echo $row['categoria']?></button></a>
                    </form>
                </ul>
            </div>
            <?php
            }
            ?>
        </div>
        <br><br>
        <?php
        if(isset($_POST['submit'])){    
            if($_POST['getId'] != ''){
                $id = $_POST['getId'];
                $news = $this->News_model->getAllNewsById($id);
                
            }
            if($_POST['getId'] == null){ 
                
            }
        }
        ?>
        <div class="card-columns">
            <?php
                $noticia = $this->News_model->getAllNews();
        
                if($noticia != null){
                foreach ($noticia as $row){?>
            <div class="card mb-3">
                <div class="card-body">
                    <p class="card-text"><?php echo ($row['fecha']);?></p>
                </div>
                <div class="card-body">
                    <h5 class="card-title"><?php echo ($row['titulo']);?></h5>
                    <p class="card-text"><?php echo ($row['descripcion']);?></p>
                    <h6 class="card-title"><?php 
                    foreach($categoria as $rowss){
                    if($row['id_categoria'] == $rowss['id']){
                    echo ($rowss['categoria']);
                        }
                    }
                    ?>

                    </h6>

                </div>
                <div class="card-footer">
                    <a href="<?php echo ($row['link']);?>" class="card-link">Ver Noticia</a>
                </div>
            </div>
            <?php
                }
            }if($news == null){
                
            }
                ?>
        </div>
    </div>

    <br><br>
    <hr>
    <div class="footer">
        <div class="fintxt">
            <a>MyCover</a>
            <a href="">|</a>
            <a>About</a>
            <a href="">|</a>
            <a>Help</a>
        </div>
    </div>
    <br>
    <p style="margin-right: 150px" class="final"><span class="logo">&copy;</span>My News Cover</p>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

</body>

</html>