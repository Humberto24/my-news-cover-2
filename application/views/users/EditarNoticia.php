<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/AgregarNoticia.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        
</head>

<body>
    <div class="arriba">
        <img class="logo" src="<?php echo base_url()?>img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php 
                    $user = $_SESSION['usuarios'];
                    foreach($user as $row){
                        echo($row['nombre']);
                    }
                ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo site_url(['user','categoria']);?>">Categories</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','noticia']);?>">News Sources</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','login']);?>">Logout</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="texto1">
            <h1 class="text">News Source</h1>
            <hr>
        </div>

        <?php
             foreach ($nombre as $row){?>
        <?php
        }
        ?>

        <form method="POST" name="form-work" action="<?php echo site_url(['news','edit', $row['id']]);?>">
            <input value="<?php echo $row['nombre'];?>" type="text" name="name" placeholder="Name">
            <br><br><br>
            <input value="<?php echo $row['URL'];?>" class="largo" type="text" name="link" placeholder="RSS URL">
            <br><br><br>

            <div class="form-group">
                <div class="col-md-6">
                    <select name="idFuente" class="form-control" data-live-search="true">
                    <?php
                        $categoria = $this->News_model->getAllCategories();
                        $data['categoria'] = $categoria;
                        foreach ($categoria as $row){?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['categoria']; ?></option>
                        <?php
                      }
                      ?>
                    </select>
                </div>
            </div>

            <br><br><br>
            <hr class="second">
            <br>
            <button type="submit" class="button">Save</button>

            <br><br><br><br>
        </form>
    </div>

    <br><br>
    <hr>
    <div class="footer">
        <div class="fintxt">
            <a>MyCover</a>
            <a href="">|</a>
            <a>About</a>
            <a href="">|</a>
            <a>Help</a>
            <br><br>
        </div>
    </div>
    <p style="margin-right: 60px" class="final"><span class="logo">&copy;</span>My News Cover</p>
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>
</body>

</html>