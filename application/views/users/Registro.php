<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registro de Usuarios</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/Registro.css">
    
</head>
<body>
    <div class="arriba">
        <img style="margin-left: 120px" class="logo" src="<?php echo base_url()?>img/logo.jpg" alt="logo">
		<button class="botonInicial">Login</button>
	</div>
	<div class="container">
		<div class="texto1">
			<h1 class="text">User Registration</h1>
			<hr class="firstLine"><br>
		</div>


<form method="POST" name="form-work" action="<?php echo site_url('user/register');?>">
    <input type="text" name="nombre" placeholder="First name">
    <input type="text" name="apellidos" placeholder="Last Name">    
    <br><br>
    <input type="text" name="email" placeholder="Email Address">
    <input type="text" name="contrasena" placeholder="Password">
    <br><br>
    <input class="large" type="text" name="direccion1" placeholder="Address">
    <br><br>
    <input class="large" type="text" name="direccion2" placeholder="Address2">
    <br><br>
    <input type="text" name="pais" placeholder="Country">
    <input type="text" name="ciudad" placeholder="City">
    <br><br>
    <input type="text" name="postal" placeholder="Zip/Postal Code">
    <input type="text" name="telefono" placeholder="Phone Number">
   <br><br><br><br><br>
   <hr class="secondLine">
   <br>

    <button type="submit" class="btn1">Sign up</button>
    <br><br><br>  
</form>

    
    
    <hr>
	<div class="footer">
		<div class="fintxt">
			<a>MyCover</a>
            <a href="">|</a>
			<a>About</a>
            <a href="">|</a>
			<a>Help</a>
		</div>
	</div>
	<p style="margin-right: 150px" class="final"><span class="logo">&copy;</span>My News Cover</p>
</body>
</html>