<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
  <div class="container-fluid">
    <div class="jumbotron">
      <h1 class="display-4">Login</h1>
      <p class="lead">Welcome to my News Cover </p>
      <hr class="my-4">
    </div>
    <form method="post" action=" <?php echo site_url('user/authenticate'); ?>">
      <div class="form-group">
        <label for="username">Username/Email</label>
        <input id="email" class="form-control" type="text" name="email">
      </div>
      <div class="form-group">
        <label for="contrasena">Password</label>
        <input id="contrasena" class="form-control" type="password" name="contrasena">
      </div>
      <p>If you don't have an account <a href="<?php echo site_url(['user','registro']);?>">Signup here</a></p>
      <br><br>
      <button type="submit" class="btn btn-primary"> Login </button>
    </form>
    <br><br>
  </div>
</body>
</html>