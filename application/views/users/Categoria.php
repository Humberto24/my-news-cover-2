<!DOCTYPE html>
<html lang="en">

<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>css/Categoria1.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>

    <div class="arriba" style="margin-top: 40px">
        <img style="margin-left: 90px" class="logo" src="<?php echo base_url()?>img/logo.jpg" alt="logo">
        <div class="dropdown">
            <button style="margin-left: 800px; margin-top: 10px" class="btn btn-secondary dropdown-toggle" type="button"
                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?php 
                    $user = $_SESSION['usuarios'];
                    foreach($user as $row){
                        echo($row['nombre']);
                    }
                ?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo site_url(['user','categoria']);?>">Categories</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','noticia']);?>">News Sources</a>
                <a class="dropdown-item" href="<?php echo site_url(['user','login']);?>">Logout</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="texto1">
            <h1 class="text">Categories</h1>
            <hr>
        </div>

        <div>

            <center>
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">Nombre</th>
                            <th scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody id="datos">
                    <?php
                    foreach ($categoria as $row){?>
                        <tr>
                            <td><?php echo $row['categoria'];?></td>
                            <td>
                                <a href="<?php echo site_url(['category','chargeCategoriesById',$row['id']]);?>"><button type="button"
                                        class="btn btn-success">Edit</button></a>
                                <a href="<?php echo site_url(['category','delete',$row['id']]);?>"><button type="button"
                                        class="btn btn-danger">Delete</button></a>
                            </td>
                        </tr>
                    </tbody>

                    <?php
        }
        ?>
                </table>
            </center>

            <div class="cuerpo">
                <button style="background-color: #283e3e;; width: 150px; height: 40px"><a style="color:white; text-decoration: none" href="<?php echo site_url(['user','agregarCategoria']);?>">Add New</a></button>
                <br><br><br><br><br><br>
            </div>
        </div>

        <footer>
        <nav>
            <hr>
            <div class="container text-center">
                <div class="content-fooa">
                    <h6> <a style="text-decoration: none; color: black" href="">My Cover</a> | <a style="text-decoration: none; color: black" href="">About</a> | <a style="text-decoration: none; color: black" href="">Help</a></h6>
                </div>
                <p class="navbar-text col-md-12 col-sm-12 col-xs-12"><span style="background-color: black; color: white;" class="logo">&copy;</span> My News Cover</p>
            </div>
        </nav>
        </footer>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
        </script>
</body>

</html>