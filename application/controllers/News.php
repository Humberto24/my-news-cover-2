<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {
    
    public function registerNewsSource(){
		$result = $this->News_model->registerNewsSource($this->input->post());
		
		if($result) {
		  $this->session->set_flashdata('msg', 'User created, please login');
		  redirect(site_url(['user','noticia']));
		} else {
			$this->session->set_flashdata('msg', 'There was an error');
		  	redirect(site_url(['user','AgregarFuenteNoticia']));
		}
	}

	public function chargeNewsById($id){
		$nombre = $this->News_model->getNewsById($id);
		$data['nombre'] = $nombre;

		$this->load->view('users/EditarNoticia', $data);
	}

    // public function chargeNewsByFilter($filter){
	// 	$nombre = $this->News_model->getNewsByFilter($filter);
	// 	$data['nombre'] = $nombre;

	// 	$this->load->view('users/dashboard', $data);
	// }

    public function getNews(){
		$noticia = $this->News_model->getAllNews();
		$data['noticias'] = $noticia;

		$this->load->view('users/dashboard', $data);
	}

    public function getNewsById(){
		$noticia = $this->News_model->getAllNewsById();
		$data['noticias'] = $noticia;

		$this->load->view('users/dashboard', $data);
	}

	public function listCategory(){
		$nombre = $this->News_model->getAll();
		$data['nombre'] = $nombre;

		$this->load->view('users/Noticia', $data);
	}

	public function chargeNews()
    {
        $titulo = $this->News_model->getNews();
        $data['titulo'] = $titulo;

        $news = $this->News_model->deleteNews();

        foreach ($titulo as $rowss) {
            $url = $rowss['URL'];
            $category = $rowss['id_categoria'];
            $user_id = $rowss['id_usuario'];
            $id_newssource = $rowss['id'];

            $invalidurl = false;
            if (@simplexml_load_file($url)) {
                $feeds = simplexml_load_file($url);
            } else {
                $invalidurl = true;
            }

            $i = 0;
            if (!empty($feeds)) {

                $site = $feeds->channel->title;
                $sitelink = $feeds->channel->link;

                foreach ($feeds->channel->item as $item) {

                    $title = $item->title;
                    $link = $item->link;
                    $description = $item->description;
                    $postDate = $item->pubDate;
                    $pubDate = date('D, d M Y', strtotime($postDate));

                    //$insertar = "INSERT INTO news VALUES ('','$title','$description','$link', '$pubDate', $id_newssource, $user_id, $category) ";
                    $result = $this->News_model->registerNews($title,$description,$link, $pubDate, $id_newssource, $user_id, $category);

                    if ($i >= 16) {       
                        break;
                    }
                    $i++;
                }
            }

        }
        redirect(site_url(['news', 'getNews']));
    }

	public function edit($id){
		$name = $this->input->post('name');
		$URL = $this->input->post('link');
		$id_categoria = $this->input->post('idFuente');
        $nombre = $this->News_model->update($id, $name, $URL, $id_categoria);
		$data['nombre'] = $nombre;
		redirect(site_url(['news','listCategory']));
    }

	public function delete($id) {
        $query = $this->News_model->delete($id);
        redirect(site_url(['news','listCategory']));
    }

	public function chargeCategories(){
		$categoria = $this->News_model->getAllCategories();
		$data['categoria'] = $categoria;

		$this->load->view('users/AgregarFuenteNoticia', $data);
	}

}