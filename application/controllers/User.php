<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		$this->load->view('users/login');
	}


  public function dashboard(){
    $this->load->view('users/dashboard');
  }

  public function agregarCategoria(){
    $this->load->view('users/AgregarCategoria');
  }

  public function agregarFuenteNoticia(){
    $this->load->view('users/AgregarFuenteNoticia');
  }

  public function categoria(){
    redirect(site_url(['category','chargeCategories']));
  }

  public function noticia(){
    redirect(site_url(['news','listCategory']));
  }

  public function editarCategoria(){
    $this->load->view('users/EdtiarCategoria');
  }

  public function editarNoticia(){
    $this->load->view('users/EditarNoticia');
  }

  public function registro(){
    $this->load->view('users/registro');
  }

  /**
   * Method for authenticating an user
   */
	public function authenticate()
	{
		$username = $this->input->post('email');
		$password = $this->input->post('contrasena');

		$user = $this->User_model->authenticate($username, $password);

		if($user) {
			session_start();
			$_SESSION['usuarios'] = $user;
			redirect('news/getNews');
		} else {
			redirect('user/login');
		}
	}

	public function chargeUserById($id){
		$user = $this->User_model->getUserById($id);
		$data['nombre'] = $user;

		$this->load->view('users/dashboard', $data);
	}

	public function register(){
		$result = $this->User_model->register($this->input->post());
	
		if($result) {
		  $this->session->set_flashdata('msg', 'User created, please login');
		  redirect(site_url(['user','login']));
		} else {
			$this->session->set_flashdata('msg', 'There was an error');
		  	redirect(site_url(['user','registro']));
		}
	  }

	  
}
