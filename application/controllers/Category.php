<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
    
	public function registerCategory(){
		$result = $this->Category_model->registerCategory($this->input->post());	
	
		if($result){
		  $this->session->set_flashdata('msg', 'User created, please login');
		 	 redirect(site_url(['category','chargeCategories']));
		} else {
			$this->session->set_flashdata('msg', 'There was an error');
		  	redirect(site_url(['user','registro']));
	    }
	}

	public function delete($id) {
        $query = $this->Category_model->delete($id);
        redirect(site_url(['category','chargeCategories']));
    }

	public function edit($id){
		$nombre = $this->input->post('category');
        $categoria = $this->Category_model->update($id, $nombre);
		$data['categoria'] = $categoria;
		redirect(site_url(['category','chargeCategories']));
    }

	/**
	 * Carga las categorias por id para editarlas
	 */
	public function chargeCategoriesById($id){
		$categoria = $this->Category_model->getCategoriesById($id);
		$data['categoria'] = $categoria;

		$this->load->view('users/EditarCategoria', $data);
	}

	/**
	 * Carga las todas las categorias para mostrarlas en categorias
	 */
	public function chargeCategories(){
		$categoria = $this->Category_model->getAllCategories();
		$data['categoria'] = $categoria;

		$this->load->view('users/Categoria', $data);
	}
}