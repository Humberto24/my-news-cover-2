<?php

class News_model extends CI_Model {

    public function registerNewsSource($new){
        $query = $this->db->insert('fuente_noticias', $new);
    
        if ($this->db->affected_rows() > 0) {
          return true;
        } else {
          return false;
        }
      }

      public function registerNews($title,$description,$link, $fecha, $id_noticia, $id_usuario, $category){
        $data = array(
          'titulo' => $title,
          'descripcion' => $description,
          'link' => $link,
          'fecha' => $fecha,
          'id_fuente_noticias' => $id_noticia,
          'id_usuario' => $id_usuario,
          'id_categoria' => $category,
      );

      $query = $this->db->insert('noticias', $data);

        if ($this->db->affected_rows() > 0) {
          return true;
        } else {
          return false;
        }
      }

      public function chargeNewsByFilter($filter){
        $this->db->like('titulo', $filter);
        $query = $this->db->get('noticias');
        return $query->result_array();
      }

      public function getAllCategories() {
        $query = $this->db->get('categorias');
        return $query->result_array();
      }

      public function getAllNewsById($id) {
        $this->db->where('id_categoria', $id);
        $query = $this->db->get('noticias');
        return $query->result_array();
      }

      public function getAllNews() {
        $query = $this->db->get('noticias');
        return $query->result_array();
      }

      public function getNewsById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('fuente_noticias');
        return $query->result_array();
      }

      public function getNews() {
        $query = $this->db->get('fuente_noticias');
        return $query->result_array();
      }

      public function getAll(){
        $this->db->select('a.*,d.categoria');
        $this->db->from('fuente_noticias a');
        $this->db->join('categorias d', 'a.id_categoria = d.id');

        $aResult = $this->db->get();

        if (!$aResult->num_rows() == 1) {
            return false;
        }

        return $aResult->result_array();
      }

      public function update($id, $nombre, $URL, $id_categoria)
      {
        $this->db->where('id', $id);
        $this->db->set('nombre', $nombre);
        $this->db->set('URL', $URL);
        $this->db->set('id_categoria', $id_categoria);
        return $this->db->update('fuente_noticias');
      }

      public function delete($id) {
        $this->db->delete('fuente_noticias', array('id'=>$id));
      }

      public function deleteNews() {
        $this->db->empty_table('noticias');
      }


}