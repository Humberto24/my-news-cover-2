<?php

class Category_model extends CI_Model {

    public function registerCategory($category){
        $query = $this->db->insert('categorias', $category);
    
        if ($this->db->affected_rows() > 0) {
          return true;
        } else {
          return false;
        }
      }

      public function getAllCategories(){
        $query = $this->db->get('categorias');
        return $query->result_array();
      }

      public function getCategoriesById($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('categorias');
        return $query->result_array();
      }

      public function update($id, $category)
      {
        $this->db->where('id', $id);
        $this->db->set('categoria', $category);
        return $this->db->update('categorias');
      }


      public function delete($id) {
        $this->db->delete('categorias', array('id'=>$id));
      }
}