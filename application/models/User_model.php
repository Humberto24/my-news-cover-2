<?php

class User_model extends CI_Model {


  public function authenticate($username, $password) {
    $this->db->where('email', $username);
    $this->db->where('contrasena', $password);
    $query = $this->db->get('usuarios');
    
    return $query->result_array();
  }

  public function getUserById($id) {
    $this->db->where('id', $id);
    $query = $this->db->get('usuarios');
    return $query->result_array();
  }

  public function register($user){
    $query = $this->db->insert('usuarios', $user);

    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

}